package com.example.practicaexamen.ViewModels;

import com.example.practicaexamen.ClassDiscos;

import java.util.ArrayList;

public class Mostrar_Discos {

    public String estado;
    public String detalle;
    public ArrayList<ClassDiscos> discos;

    public Mostrar_Discos(){

    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<ClassDiscos> getDiscos() {
        return discos;
    }

    public void setDiscos(ArrayList<ClassDiscos> discos) {
        this.discos = discos;
    }
}
