package com.example.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicaexamen.ViewModels.Mostrar_Discos;
import com.example.practicaexamen.api.Api;
import com.example.practicaexamen.api.Servicios.ServicioPeticion;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Discos extends AppCompatActivity {

    ArrayList<String> C_Nombre = new ArrayList<>();
    ArrayList<String> C_Id = new ArrayList<>();
    ArrayList<String> C_Album = new ArrayList<>();
    ArrayList<String> C_Anio = new ArrayList<>();

    public ListView listaNombres;
    public TextView TVid, TVnombre, TValbum, TVanio, TVcerrar;

    public String Detalle_id[];
    public String D_Album[];
    public String D_Anio[];



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discos);

        getSupportActionBar().hide();

        TVid = (TextView)findViewById(R.id.tv_id);
        TVnombre = (TextView)findViewById(R.id.tv_nombre);
        TValbum = (TextView)findViewById(R.id.tv_album);
        TVanio = (TextView)findViewById(R.id.tv_anio);
        TVcerrar = (TextView)findViewById(R.id.tv_cerrar);
        listaNombres = (ListView)findViewById(R.id.list);


        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,C_Nombre);

        listaNombres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TVnombre.setText("Nombre: " + listaNombres.getItemAtPosition(position));
                TVid.setText("ID: " + Detalle_id[position]);
                TValbum.setText("Album: " + D_Album[position]);
                TVanio.setText("Año: " + D_Anio[position]);

            }
        });





        ServicioPeticion service = Api.getApi(Discos.this).create(ServicioPeticion.class);
        Call<Mostrar_Discos> UsuariosCall = service.getDiscosP();
        UsuariosCall.enqueue(new Callback<Mostrar_Discos>() {
            @Override
            public void onResponse(Call<Mostrar_Discos> call, Response<Mostrar_Discos> response) {
                Mostrar_Discos peticion = response.body();

                if(peticion.estado.equals("true")){

                    List<ClassDiscos> Pe_Discos = peticion.discos;

                    for (ClassDiscos mostrar : Pe_Discos){
                        C_Nombre.add(mostrar.getNombre());
                    }
                    listaNombres.setAdapter(arrayAdapter);

                    for (ClassDiscos mostrar : Pe_Discos){
                        C_Id.add(mostrar.getId());
                    }
                    Detalle_id = C_Id.toArray(new String[C_Id.size()]);

                    for (ClassDiscos mostrar : Pe_Discos){
                        C_Album.add(mostrar.getAlbum());
                    }
                    D_Album = C_Album.toArray(new String[C_Album.size()]);

                    for (ClassDiscos mostrar : Pe_Discos){
                        C_Anio.add(mostrar.getAnio());
                    }
                    D_Anio = C_Anio.toArray(new String[C_Anio.size()]);



                }else {

                    Toast.makeText(Discos.this,"Error",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Mostrar_Discos> call, Throwable t) {

                Toast.makeText(Discos.this,"Error",Toast.LENGTH_LONG).show();

            }
        });

        TVcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                String token = "";
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN", token);
                editor.commit();

                Intent intent1 = new Intent(Discos.this, MainActivity.class);
                startActivity(intent1);
            }
        });


    }
}
